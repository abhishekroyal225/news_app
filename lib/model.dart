class Articles{
  String strAuthor      = '';
  String strTitle       = '';
  String strDescription = '';
  String strUrl         = '';
  String strUrlToImage  = '';
  String strPublishedAt = '';
  String strContent     = '';
  Source source;
  Articles({
    this.strAuthor,
    this.strTitle,
    this.strDescription,
    this.strUrl,
    this.strUrlToImage ,
    this.strPublishedAt,
    this.strContent,
    this.source,
  });

  factory Articles.fromJSON(Map<String, dynamic> parsedJSON){
    return Articles(
      strAuthor     : parsedJSON['author'],
      strTitle      : parsedJSON['title'],
      strDescription: parsedJSON['description'],
      strUrl        : parsedJSON['url'],
      strUrlToImage : parsedJSON['urlToImage'],
      strPublishedAt: parsedJSON['publishedAt'],
      strContent    : parsedJSON['content'],
      source        : Source.fromJSON(parsedJSON['source']), 
    );
  }
}

class Source{
  String strId    = '';
  String strName  = '';

  Source({
    this.strId,
    this.strName,
  });

  factory Source.fromJSON(Map<String, dynamic> parsedJSON){
    return Source(
      strId   : parsedJSON['id'],
      strName : parsedJSON['name'],
    );
  }
}