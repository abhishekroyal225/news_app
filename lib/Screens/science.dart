import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:news_app/article_details.dart';
import 'package:news_app/common_design.dart';
import 'package:news_app/news_main_api.dart';

class Science extends StatefulWidget {
  double dHeight = 0, dWidth = 0;
  Science({
    this.dHeight,
    this.dWidth,
  });
  @override
  _ScienceState createState() => _ScienceState();
}

class _ScienceState extends State<Science> {
  NewsMainAPI newsMainAPI = NewsMainAPI();

  @override
  void initState() {
    super.initState();
    newsMainAPI.getScienceData();
  }

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (context) {
        return Container(
          child: (newsMainAPI.scienceList != null)
              ? ListView.builder(
                  itemCount: newsMainAPI.scienceList.length,
                  itemBuilder: (context, index) {
                    return CommonDesign(
                      dHeight: widget.dHeight,
                      dWidth: widget.dWidth,
                      apiData: newsMainAPI.scienceList[index],
                    );
                  })
              : Container(
                  width: 40,
                  child: Center(
                    child: CircularProgressIndicator(
                      valueColor:
                          AlwaysStoppedAnimation<Color>(Colors.blueAccent),
                    ),
                  ),
                ),
        );
      },
    );
  }
}
