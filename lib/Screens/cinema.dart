import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:news_app/common_design.dart';
import 'package:news_app/news_main_api.dart';

class Cinema extends StatefulWidget {
  double dHeight = 0, dWidth = 0;
  Cinema({
    this.dHeight,
    this.dWidth,
  });
  @override
  _CinemaState createState() => _CinemaState();
}

class _CinemaState extends State<Cinema> {
  NewsMainAPI newsMainAPI = NewsMainAPI();

  @override
  void initState() {
    super.initState();
    newsMainAPI.getCinemaData();
  }

  @override
  Widget build(BuildContext context) {
    return Observer(builder: (context) {
      return Container(
        child: (newsMainAPI.cinemaList.length != 0)
            ? ListView.builder(
                itemCount: newsMainAPI.cinemaList.length,
                itemBuilder: (context, index) {
                  return CommonDesign(
                    dHeight: widget.dHeight,
                    dWidth: widget.dWidth,
                    apiData: newsMainAPI.cinemaList[index],
                  );
                })
            : Container(
                width: 40,
                child: Center(
                  child: CircularProgressIndicator(
                    valueColor:
                        AlwaysStoppedAnimation<Color>(Colors.blueAccent),
                  ),
                ),
              ),
      );
    });
  }
}
