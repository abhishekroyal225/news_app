import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:news_app/article_details.dart';
import 'package:news_app/common_design.dart';
import 'package:news_app/news_main_api.dart';

class Business extends StatefulWidget {
  double dHeight = 0.0, dWidth = 0.0;
  Business({this.dHeight, this.dWidth});

  @override
  _BusinessState createState() => _BusinessState();
}

class _BusinessState extends State<Business> {
  NewsMainAPI newsMainAPI = NewsMainAPI();

  @override
  void initState() {
    super.initState();
    newsMainAPI.getBusinessData();
  }

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (context) {
        return Container(
          child: (newsMainAPI.businessList.length != 0)
              ? ListView.builder(
                  itemCount: newsMainAPI.businessList.length,
                  itemBuilder: (context, index) {
                    return CommonDesign(
                      dHeight: widget.dHeight,
                      dWidth: widget.dWidth,
                      apiData: newsMainAPI.businessList[index],
                    );
                  },
                )
              : Container(
                  width: 40,
                  child: Center(
                    child: CircularProgressIndicator(
                      valueColor:
                          AlwaysStoppedAnimation<Color>(Colors.blueAccent),
                    ),
                  ),
                ),
        );
      },
    );
  }
}
