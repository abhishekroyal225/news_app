import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:news_app/article_details.dart';
import 'package:news_app/common_design.dart';
import 'package:news_app/news_main_api.dart';

class Technology extends StatefulWidget {
  double dHeight = 0, dWidth = 0;
  Technology({
    this.dHeight,
    this.dWidth,
  });
  @override
  _TechnologyState createState() => _TechnologyState();
}

class _TechnologyState extends State<Technology> {
  NewsMainAPI newsMainAPI = NewsMainAPI();
  @override
  void initState() {
    super.initState();
    newsMainAPI.getTechnologyData();
  }

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (context) {
        return Container(
          child: (newsMainAPI.technologyList.length != 0)
              ? ListView.builder(
                  itemCount: newsMainAPI.technologyList.length,
                  itemBuilder: (context, index) {
                    return CommonDesign(
                      dHeight: widget.dHeight,
                      dWidth: widget.dWidth,
                      apiData: newsMainAPI.technologyList[index],
                    );
                  },
                )
              : Container(
                  width: 40,
                  child: Center(
                    child: CircularProgressIndicator(
                      valueColor:
                          AlwaysStoppedAnimation<Color>(Colors.blueAccent),
                    ),
                  ),
                ),
        );
      },
    );
  }
}
