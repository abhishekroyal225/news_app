import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:news_app/article_details.dart';
import 'package:news_app/common_design.dart';
import 'package:news_app/news_main_api.dart';

class Health extends StatefulWidget {
  double dHeight = 0, dWidth = 0;
  Health({
    this.dHeight,
    this.dWidth,
  });
  @override
  _HealthState createState() => _HealthState();
}

class _HealthState extends State<Health> {
  NewsMainAPI newsMainAPI = NewsMainAPI();

  @override
  void initState() {
    super.initState();
    newsMainAPI.getHealthData();
  }

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (context) {
        return Container(
          child: (newsMainAPI.sportsList.length != 0)
              ? ListView.builder(
                  itemCount: newsMainAPI.healthList.length,
                  itemBuilder: (context, index) {
                    return CommonDesign(
                      dHeight: widget.dHeight,
                      dWidth: widget.dWidth,
                      apiData: newsMainAPI.healthList[index],
                    );
                  })
              : Container(
                  width: 40,
                  child: Center(
                    child: CircularProgressIndicator(
                      valueColor:
                          AlwaysStoppedAnimation<Color>(Colors.blueAccent),
                    ),
                  ),
                ),
        );
      },
    );
  }
}
