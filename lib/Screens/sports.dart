import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:news_app/article_details.dart';
import 'package:news_app/common_design.dart';
import 'package:news_app/news_main_api.dart';

class Sports extends StatefulWidget {
  double dHeight = 0, dWidth = 0;
  Sports({this.dHeight, this.dWidth});

  @override
  _SportsState createState() => _SportsState();
}

class _SportsState extends State<Sports> {
  NewsMainAPI newsMainAPI = NewsMainAPI();

  @override
  void initState() {
    super.initState();
    newsMainAPI.getSportsData();
  }

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (context) {
        return Container(
          child: (newsMainAPI.sportsList != null)
              ? ListView.builder(
                  itemCount: newsMainAPI.sportsList.length,
                  itemBuilder: (context, index) {
                    return CommonDesign(
                      dHeight: widget.dHeight,
                      dWidth: widget.dWidth,
                      apiData: newsMainAPI.sportsList[index],
                    );
                  })
              : Container(
                  width: 40,
                  child: Center(
                    child: CircularProgressIndicator(
                      valueColor:
                          AlwaysStoppedAnimation<Color>(Colors.blueAccent),
                    ),
                  ),
                ),
        );
      },
    );
  }
}
