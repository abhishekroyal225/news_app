import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:news_app/news_main_api.dart';

import '../common_design.dart';

class Trending extends StatefulWidget {
  double dHeight, dWidth;
  Trending({this.dHeight, this.dWidth});

  @override
  _TrendingState createState() => _TrendingState();
}

class _TrendingState extends State<Trending> {
  NewsMainAPI newsMainAPI = NewsMainAPI();

  @override
  void initState() {
    super.initState();
    newsMainAPI.getTrendingNewsData();
  }

  @override
  Widget build(BuildContext context) {
    return Observer(builder: (context) {
      return Container(
        child: (newsMainAPI.trendingList.length != 0)
            ? ListView.builder(
                itemCount: newsMainAPI.trendingList.length,
                itemBuilder: (context, index) {
                  return CommonDesign(
                      dHeight: widget.dHeight,
                      dWidth: widget.dWidth,
                      apiData: newsMainAPI.trendingList[index]);
                })
            : Container(
                width: 40,
                child: Center(
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(Colors.blueAccent),
                  ),
                  // child: LinearProgressIndicator(
                  //   valueColor: AlwaysStoppedAnimation<Color>(Colors.blueAccent),
                  // ),
                ),
              ),
      );
    });
  }
}
