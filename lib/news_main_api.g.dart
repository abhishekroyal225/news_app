// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'news_main_api.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$NewsMainAPI on NewsMainAPIBase, Store {
  final _$cinemaListAtom = Atom(name: 'NewsMainAPIBase.cinemaList');

  @override
  List<Articles> get cinemaList {
    _$cinemaListAtom.reportRead();
    return super.cinemaList;
  }

  @override
  set cinemaList(List<Articles> value) {
    _$cinemaListAtom.reportWrite(value, super.cinemaList, () {
      super.cinemaList = value;
    });
  }

  final _$healthListAtom = Atom(name: 'NewsMainAPIBase.healthList');

  @override
  List<Articles> get healthList {
    _$healthListAtom.reportRead();
    return super.healthList;
  }

  @override
  set healthList(List<Articles> value) {
    _$healthListAtom.reportWrite(value, super.healthList, () {
      super.healthList = value;
    });
  }

  final _$scienceListAtom = Atom(name: 'NewsMainAPIBase.scienceList');

  @override
  List<Articles> get scienceList {
    _$scienceListAtom.reportRead();
    return super.scienceList;
  }

  @override
  set scienceList(List<Articles> value) {
    _$scienceListAtom.reportWrite(value, super.scienceList, () {
      super.scienceList = value;
    });
  }

  final _$sportsListAtom = Atom(name: 'NewsMainAPIBase.sportsList');

  @override
  List<Articles> get sportsList {
    _$sportsListAtom.reportRead();
    return super.sportsList;
  }

  @override
  set sportsList(List<Articles> value) {
    _$sportsListAtom.reportWrite(value, super.sportsList, () {
      super.sportsList = value;
    });
  }

  final _$technologyListAtom = Atom(name: 'NewsMainAPIBase.technologyList');

  @override
  List<Articles> get technologyList {
    _$technologyListAtom.reportRead();
    return super.technologyList;
  }

  @override
  set technologyList(List<Articles> value) {
    _$technologyListAtom.reportWrite(value, super.technologyList, () {
      super.technologyList = value;
    });
  }

  final _$trendingListAtom = Atom(name: 'NewsMainAPIBase.trendingList');

  @override
  List<Articles> get trendingList {
    _$trendingListAtom.reportRead();
    return super.trendingList;
  }

  @override
  set trendingList(List<Articles> value) {
    _$trendingListAtom.reportWrite(value, super.trendingList, () {
      super.trendingList = value;
    });
  }

  final _$businessListAtom = Atom(name: 'NewsMainAPIBase.businessList');

  @override
  List<Articles> get businessList {
    _$businessListAtom.reportRead();
    return super.businessList;
  }

  @override
  set businessList(List<Articles> value) {
    _$businessListAtom.reportWrite(value, super.businessList, () {
      super.businessList = value;
    });
  }

  final _$getTrendingNewsDataAsyncAction =
      AsyncAction('NewsMainAPIBase.getTrendingNewsData');

  @override
  Future getTrendingNewsData() {
    return _$getTrendingNewsDataAsyncAction
        .run(() => super.getTrendingNewsData());
  }

  final _$getCinemaDataAsyncAction =
      AsyncAction('NewsMainAPIBase.getCinemaData');

  @override
  Future getCinemaData() {
    return _$getCinemaDataAsyncAction.run(() => super.getCinemaData());
  }

  final _$getTechnologyDataAsyncAction =
      AsyncAction('NewsMainAPIBase.getTechnologyData');

  @override
  Future getTechnologyData() {
    return _$getTechnologyDataAsyncAction.run(() => super.getTechnologyData());
  }

  final _$getScienceDataAsyncAction =
      AsyncAction('NewsMainAPIBase.getScienceData');

  @override
  Future getScienceData() {
    return _$getScienceDataAsyncAction.run(() => super.getScienceData());
  }

  final _$getSportsDataAsyncAction =
      AsyncAction('NewsMainAPIBase.getSportsData');

  @override
  Future getSportsData() {
    return _$getSportsDataAsyncAction.run(() => super.getSportsData());
  }

  final _$getHealthDataAsyncAction =
      AsyncAction('NewsMainAPIBase.getHealthData');

  @override
  Future getHealthData() {
    return _$getHealthDataAsyncAction.run(() => super.getHealthData());
  }

  final _$getBusinessDataAsyncAction =
      AsyncAction('NewsMainAPIBase.getBusinessData');

  @override
  Future getBusinessData() {
    return _$getBusinessDataAsyncAction.run(() => super.getBusinessData());
  }

  @override
  String toString() {
    return '''
cinemaList: ${cinemaList},
healthList: ${healthList},
scienceList: ${scienceList},
sportsList: ${sportsList},
technologyList: ${technologyList},
trendingList: ${trendingList},
businessList: ${businessList}
    ''';
  }
}
