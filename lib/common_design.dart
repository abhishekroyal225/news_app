import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'article_details.dart';

class CommonDesign extends StatelessWidget {
  double dHeight, dWidth;
  var apiData;
  CommonDesign({this.dHeight, this.dWidth, this.apiData});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: dHeight * 0.26,
      width: dWidth,
      margin: EdgeInsets.only(
        top: 10,
        left: 10,
        right: 10,
      ),
      decoration: BoxDecoration(
        color: Colors.black,
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
        children: <Widget>[
          Container(
            height: dHeight * 0.2,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: (apiData.strUrlToImage != null)
                    ? NetworkImage(apiData.strUrlToImage)
                    : AssetImage('assets/image_place_holder.png'),
                fit: BoxFit.cover,
              ),
            ),
            child: Container(
              alignment: Alignment.bottomCenter,
              margin: EdgeInsets.only(bottom: 20.0),
              child: Text(
                apiData.strTitle,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  backgroundColor: Colors.black54,
                  fontSize: 15,
                ),
              ),
            ),
          ),
          Container(
            alignment: Alignment.bottomRight,
            margin: EdgeInsets.only(
              top: 10.0,
              bottom: 10.0,
              right: 10.0,
            ),
            child: InkWell(
              onTap: () => Navigator.pushAndRemoveUntil(
                  context,
                  CupertinoPageRoute(
                    builder: (context) => ArticleDetails(
                      title: 'Trending',
                      articles: apiData,
                    ),
                  ),
                  (route) => true),
              child: Text(
                'READ MORE',
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
