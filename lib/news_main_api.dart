import 'package:dio/dio.dart';
import 'package:mobx/mobx.dart';
import 'package:news_app/model.dart';
import './utils.dart' as utils;
part 'news_main_api.g.dart';

class NewsMainAPI = NewsMainAPIBase with _$NewsMainAPI;

abstract class NewsMainAPIBase with Store {
  @observable
  List<Articles> cinemaList = List<Articles>();

  @observable
  List<Articles> healthList = List<Articles>();

  @observable
  List<Articles> scienceList = List<Articles>();

  @observable
  List<Articles> sportsList = List<Articles>();

  @observable
  List<Articles> technologyList = List<Articles>();

  @observable
  List<Articles> trendingList = List<Articles>();

  @observable
  List<Articles> businessList = List<Articles>();

  @action
  getTrendingNewsData() async {
    var dio = Dio();
    Response trendingNewsResponse = await dio.get(utils.trendingDataURl);
    trendingList = trendingNewsResponse.data['articles']
        .map<Articles>((jsonData) => Articles.fromJSON(jsonData))
        .toList();
  }

  @action
  getCinemaData() async {
    var dio = Dio();
    Response cinemaResponse = await dio.get(utils.cinemaDataURl);
    cinemaList = cinemaResponse.data['articles']
        .map<Articles>((jsonData) => Articles.fromJSON(jsonData))
        .toList();
  }

  @action
  getTechnologyData() async {
    var dio = Dio();
    Response technologyResponse = await dio.get(utils.technologyDataURl);
    technologyList = technologyResponse.data['articles']
        .map<Articles>((jsonData) => Articles.fromJSON(jsonData))
        .toList();
  }

  @action
  getScienceData() async {
    var dio = Dio();
    Response scienceResponse = await dio.get(utils.scienceDataURl);
    scienceList = scienceResponse.data['articles']
        .map<Articles>((jsonData) => Articles.fromJSON(jsonData))
        .toList();
  }

  @action
  getSportsData() async {
    var dio = Dio();
    Response sportsResponse = await dio.get(utils.sportsDataURl);
    sportsList = sportsResponse.data['articles']
        .map<Articles>((jsonData) => Articles.fromJSON(jsonData))
        .toList();
  }

  @action
  getHealthData() async {
    var dio = Dio();
    Response healthResponse = await dio.get(utils.healthDataURl);
    healthList = healthResponse.data['articles']
        .map<Articles>((jsonData) => Articles.fromJSON(jsonData))
        .toList();
  }

  @action
  getBusinessData() async {
    var dio = Dio();
    Response businessResponse = await dio.get(utils.businessDataURl);
    businessList = businessResponse.data['articles']
        .map<Articles>((jsonData) => Articles.fromJSON(jsonData))
        .toList();
  }
}
