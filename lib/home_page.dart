import 'package:flutter/material.dart';
import 'package:news_app/Screens/business.dart';
import 'package:news_app/Screens/cinema.dart';
import 'package:news_app/Screens/health.dart';
import 'package:news_app/Screens/science.dart';
import 'package:news_app/Screens/sports.dart';
import 'package:news_app/Screens/technology.dart';
import 'package:news_app/Screens/trending.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  double _height = 0.0, _width = 0.0;
  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    return DefaultTabController(
      length: 7,
      child: Scaffold(
        backgroundColor: Color(0xff616C6F),
        appBar: AppBar(
          backgroundColor: Color(0xff4C4B4B),
          title: Text('InShortNews'),
          centerTitle: true,
          bottom: TabBar(
            isScrollable: true,
            indicatorColor: Colors.white70,
            indicatorPadding: const EdgeInsets.all(2.0),
            indicatorWeight: 5.0,
            tabs: <Widget>[
              Tab(
                child: Container(
                  child: Text('Trending'),
                ),
              ),
              Tab(
                child: Container(
                  child: Text('Cinema'),
                ),
              ),
              Tab(
                child: Container(
                  child: Text('Technology'),
                ),
              ),
              Tab(
                child: Container(
                  child: Text('Science'),
                ),
              ),
              Tab(
                child: Container(
                  child: Text('Sports'),
                ),
              ),
              Tab(
                child: Container(
                  child: Text('Health'),
                ),
              ),
              Tab(
                child: Container(
                  child: Text('Business'),
                ),
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            Trending(
              dHeight: _height,
              dWidth: _width,
            ),
            Cinema(
              dHeight: _height,
              dWidth: _width,
            ),
            Technology(
              dHeight: _height,
              dWidth: _width,
            ),
            Science(
              dHeight: _height,
              dWidth: _width,
            ),
            Sports(
              dHeight: _height,
              dWidth: _width,
            ),
            Health(
              dHeight: _height,
              dWidth: _width,
            ),
            Business(
              dHeight: _height,
              dWidth: _width,
            ),
          ],
        ),
      ),
    );
  }
}
