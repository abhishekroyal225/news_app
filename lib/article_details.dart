import 'package:flutter/material.dart';
import 'package:news_app/model.dart';

class ArticleDetails extends StatefulWidget {
  String title;
  Articles articles;
  ArticleDetails({this.title, this.articles});
  @override
  _ArticleDetailsState createState() => _ArticleDetailsState();
}

class _ArticleDetailsState extends State<ArticleDetails> {
  double _height = 0.0, _width = 0.0;
  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Color(0xff4C4B4B),
      appBar: AppBar(
        backgroundColor: Color(0xff4C4B4B),
        title: Text(widget.title),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: Container(
        child: ListView(
          children: <Widget>[
            Container(
              height: _height * 0.2,
              child: Image.network(
                widget.articles.strUrlToImage,
                fit: BoxFit.cover,
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                top: 5.0,
                left: 10.0,
              ),
              child: Text(
                widget.articles.strTitle,
                textAlign: TextAlign.left,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 25,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                left: 10.0,
                top: 40.0,
              ),
              child: Row(
                children: [
                  Text(
                    'Posted By: ',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      left: 5.0,
                    ),
                    child: Text(
                      widget.articles.strAuthor,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                left: 10.0,
                top: 30.0,
              ),
              child: Text(
                widget.articles.strDescription,
                textAlign: TextAlign.left,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}